package be.fenego.ishpwa.genericrestapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import be.fenego.ishpwa.genericrestapi.models.WakeTimeModel;

@RestController
@CrossOrigin
public class WebController {
	private String wakeTime = "00:00";
	private boolean disabled = false;
	
	@RequestMapping(value = "/alarm/waketime", method = RequestMethod.GET)
    public ResponseEntity<WakeTimeModel> getWakeTime() {
        if (false) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<WakeTimeModel>(getWakeTimeResponse(), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/alarm/waketime", method = RequestMethod.PUT)
	public ResponseEntity<WakeTimeModel> setWakeTime(@RequestBody WakeTimeModel requestBody) {
		wakeTime = requestBody.getWakeTime();
		disabled = requestBody.isDisabled();
        return new ResponseEntity<WakeTimeModel>(getWakeTimeResponse(), HttpStatus.OK);
    }
	
	private WakeTimeModel getWakeTimeResponse() {
		WakeTimeModel response = new WakeTimeModel();
        response.setWakeTime(wakeTime);
        response.setDisabled(disabled);
        return response;
	}
}
