package be.fenego.ishpwa.genericrestapi.models;

public class WakeTimeModel {
	private String wakeTime;
	private boolean disabled;
	
	public String getWakeTime() {
		return wakeTime;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setWakeTime(String wakeTime) {
		this.wakeTime = wakeTime;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
